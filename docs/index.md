# Home

## Quick Navigation
* [Pleroma Clients](backend/clients)
* [Installing on Linux using OTP releases](backend/installation/otp_en.md)
* [Configuration Cheat Sheet](backend/configuration/cheatsheet.md)
* [Pleroma-FE configuration and customization for instance administrators](frontend/CONFIGURATION.md)

## Community Channels
* IRC: **#pleroma** and **#pleroma-dev** on libera.chat, webchat is available at <https://irc.pleroma.social>
* Matrix: [#pleroma:libera.chat](https://matrix.to/#/#pleroma:libera.chat) and [#pleroma-dev:libera.chat](https://matrix.to/#/#pleroma-dev:libera.chat)
* Code: <https://git.pleroma.social/pleroma/>

